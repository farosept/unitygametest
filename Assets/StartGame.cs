﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour {

	// Use this for initialization
	public void StartLevel ()
	{
	    Application.LoadLevel(1);
	}

    public void Exit()
    {
        Application.Quit();
    }
}
