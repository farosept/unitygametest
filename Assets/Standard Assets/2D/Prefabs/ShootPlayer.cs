﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.UI;

public class ShootPlayer : MonoBehaviour
{
    public GameObject ammo;
    public float force = 0.1f;
    public float nextFire=0;
    public float fireRate = 1;
    public AudioClip ashot;
    public string text="None";
    public Text cc;
    // Use this for initialization
    void Start ()
    {
        
    }
	
	// Update is called once per frame
	void Update ()
	{
	    cc.text = text;
        if (Input.GetMouseButton(0) && Time.time > nextFire)
	    {
            nextFire = Time.time + fireRate;
            GetComponent<AudioSource>().PlayOneShot(ashot);
            Instantiate(ammo, transform.position,transform.rotation).GetComponent<Rigidbody2D>().AddForce(transform.up*force,ForceMode2D.Impulse);
	    }
	}
}
