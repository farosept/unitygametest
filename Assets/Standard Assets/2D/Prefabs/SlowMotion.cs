﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowMotion : MonoBehaviour
{
    private bool slowKey = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(KeyCode.C))
	    {
	        if (slowKey == false)
	        {
	            slowKey = true;
	            Time.timeScale = 0.2f;
	        }
	        else
	        {
                slowKey = false;
                Time.timeScale = 1f;
            }
	    }
	}
}
