﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    public GameObject Player;
    private Vector3 m_CurrentVelocity;
    public float smooth = 2;

    private float height=20;
    public float maxHeight = 20;
    public float minHeight = 10;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
        Vector3 mouse = Input.mousePosition;

        // находим позицию персонажа в пространстве экрана
        Vector3 playerPos = Camera.main.WorldToScreenPoint(Player.transform.position);

        // создаем окно, уменьшившую в два раза копию текущего, и закрепляем его за позицией персонажа
        Rect rect = new Rect(playerPos.x - (Screen.width / 2) / 2, playerPos.y - (Screen.height / 2) / 1, Screen.width / 2, Screen.height);

        // удерживание позиции курсора в рамках окна
        mouse = Vector2.Max(mouse, rect.min);
        mouse = Vector2.Min(mouse, rect.max);

        // переносим позицию курсора в мировые координаты
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(mouse.x, mouse.y, transform.position.y));

        // находим центр, между персонажем и измененной позиций курсора
        Vector3 camLook = (Player.transform.position + mousePos) / 2;

        // финальная позиция камеры
        transform.position = Vector3.Lerp(transform.position, new Vector3(camLook.x, camLook.y, -10), smooth * Time.deltaTime);
    }
}

