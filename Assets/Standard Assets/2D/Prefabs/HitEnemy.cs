﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitEnemy : MonoBehaviour {
    
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void OnTriggerEnter2D(Collider2D collider)
    {
        Destroy(gameObject);
        if (collider.gameObject.tag == "Player")
            collider.gameObject.GetComponent<PlayerInfo>().HP -= 1;
    }
}
