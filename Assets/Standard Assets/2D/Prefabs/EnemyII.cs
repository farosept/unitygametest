﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyII : MonoBehaviour
{

    public GameObject target;
    public GameObject ammo;
    public float force=2000;
    public int HP = 3;
    private int t=0;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
	    RaycastHit2D hit,wallHit;
	    bool trigger = true;
	    var Distance = Vector3.Distance(target.transform.position, transform.position);
        if (HP <= 0) { Destroy(gameObject);}
        var up = transform.TransformDirection(Vector3.up);
	    if ((hit = Physics2D.Raycast(transform.position, up, 30, 1 << 0)) || Distance <= 20f)
	    {
	        Debug.DrawRay(transform.position, up * 30, Color.green);
	        if (t % 20 == 0)
	        {
	            Shoot();
	        }
	        t++;
	        var angle = Vector2.Angle(Vector2.up, target.transform.position - transform.position);
	        //угол между вектором от объекта к мыше и осью х
	        transform.eulerAngles = new Vector3(0f, 0f,
	            transform.position.x < target.transform.position.x ? -angle : angle);
	    }
	}
    void Shoot()
    {
        Instantiate(ammo, transform.position, transform.rotation).GetComponent<Rigidbody2D>().AddForce(transform.up * force);
    }
}
