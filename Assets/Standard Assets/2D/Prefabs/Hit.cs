﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hit : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame

    public void OnTriggerEnter2D(Collider2D collider)
    {
        Destroy(gameObject);
        if(collider.gameObject.tag=="Enemy")
        collider.gameObject.GetComponent<EnemyII>().HP -= 1;
    }
}
