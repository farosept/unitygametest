﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rifle : MonoBehaviour {
    public GameObject ammo;
    public float force = 15f;
    public float fireRate = 0.1f;
    public AudioClip ashot;
    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    public void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log("Contact rfle");
        if (collider.gameObject.tag == "Player")
        {
            Destroy(gameObject);
            var ShootPlayer = collider.gameObject.GetComponent<ShootPlayer>();
            ShootPlayer.ammo = ammo;
            ShootPlayer.fireRate = fireRate;
            ShootPlayer.force = force;
            ShootPlayer.ashot = ashot;
            ShootPlayer.text = "Rifle";
        }
    }
}
