﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pistol : MonoBehaviour
{
    public AudioClip ashot;
    public GameObject ammo;
    public float force = 10f;
    public float fireRate = 0.5f;
    // Use this for initialization
    void Start () {
		
	}
    public void OnTriggerEnter2D(Collider2D collider)
    {
       Debug.Log("Contact;");
        if (collider.gameObject.tag == "Player")
        {
            Destroy(gameObject);
            var ShootPlayer = collider.gameObject.GetComponent<ShootPlayer>();
            ShootPlayer.ammo = ammo;
            ShootPlayer.fireRate = fireRate;
            ShootPlayer.force = force;
            ShootPlayer.ashot = ashot;
            ShootPlayer.text = "Pistol";
        }
    }
}
